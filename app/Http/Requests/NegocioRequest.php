<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NegocioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'servicio1' => 'required|min:3|max:50',
            'servicio2' => 'required|min:3|max:50',
            'servicio3' => 'required|min:3|max:50',
            'descripcion' => 'required|min:3|max:50',
            'contacto' => 'required|min:3|max:50'
            
        ];
    }
    public function messages()
    {
        return [
            'servicio1.required' => 'El servicio es requerido',
            'servicio2.required' => 'El servicio es requerido',
            'servicio3.required' => 'El servicio es requerido',
            'descripcion.required' => 'Descripcion requerida',
            'contacto.required' => 'El contacto es requerido'
          
        ];
    }
}
