<?php

namespace App\Http\Controllers;
use App\Http\Requests\ComercioRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Comercio;
use App\Models\User;
use App\Models\Comerciodetalle;
Use Session;
Use Redirect;
class ComercioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('post_index'), 403);

        $posts = Comercio::join('users', 'comercios.id_user','=','users.id')
        ->join('estados','comercios.id_estado','=','estados.id')
        ->select('comercios.id as id'
                ,'comercios.nombre as nombre'
                ,'comercios.telefono as telefono'
                ,'estados.estado as estado'
                ,'users.name as name')->paginate(5);
         return view('comercios.index', compact('posts'));
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
         return view('comercios.create', compact('users')); 
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComercioRequest $request)
    {

        Comercio::create($request->all());
       
        $data = Comercio::latest()->first('id');
        
          Comerciodetalle::create([
            'id_comercio' => $data['id'] ]);
        

        return redirect()->route('comercios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comercio $comercio)
    {   
              return view('comercios.show', compact('comercio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comercio = Comercio::join('users', 'comercios.id_user','=','users.id')
        ->join('estados','comercios.id_estado','=','estados.id')
        ->select('comercios.id as id'
                ,'comercios.nombre as nombre'
                ,'comercios.telefono as telefono'
                ,'estados.id as id_estado'
                ,'estados.estado as estado'
                ,'users.name as name')->where('comercios.id','=', $id)->first();
        return view('comercios.edit', compact('comercio'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComercioRequest $request, Comercio $comercio)
    {
        $comercio->update($request->all());

        return redirect()->route('comercios.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comercio $comercio)
    {
        try {
            $comercio->delete();
            
        } catch (\Throwable $th) {
            Session::flash('message', 'Error de eliminacion');
            return redirect()->route('comercios.index');
        }

        return redirect()->route('comercios.index');
    }
}
