<?php

namespace App\Http\Controllers;
use App\Models\Comercio;
use App\Models\Comerciodetalle;
use App\Models\Encuesta;
use Illuminate\Http\Request;

class TuristaController extends Controller
{
    public function negocios()
    {
        $negocios = Comerciodetalle::join('comercios', 'comerciodetalles.id_comercio','=','comercios.id')
        ->select('comercios.id as id'
                ,'comercios.nombre as nombre'
                ,'comercios.telefono as telefono'
                ,'comerciodetalles.id as id_detalle'
                ,'comerciodetalles.servicio1 as servicio1'
                ,'comerciodetalles.servicio2 as servicio2'
                ,'comerciodetalles.servicio3 as servicio3'
                ,'comerciodetalles.descripcion as descripcion'
                ,'comerciodetalles.contacto as contacto'
                ,'comerciodetalles.ruta as ruta'
                )->where('id_estado','=',1)->orderBy('nombre','ASC')->paginate(5);
                 
        return view('turismo.negocios', compact('negocios') );
    }
    public function ubicacion()
    {
        return view('turismo.ubicacion');
    }

    public function about()
    {
        return view('turismo.about');
    }

    public function virtual()
    {
        return view('turismo.virtual');
    }

    public function encuesta(Request $request)
    {
        $request->validate([
            'id_votos' => 'required'
        ]);

        $new = new Encuesta();
        $new->id_votos = $request->id_votos;
        $new->save();


        return response()->json('exito');
       
    }
}
