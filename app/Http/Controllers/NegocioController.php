<?php

namespace App\Http\Controllers;

use App\Http\Requests\NegocioRequest;
use Illuminate\Http\Request;
use App\Models\Comercio;
use App\Models\User;
use App\Models\Comerciodetalle;
use Validator;
use File;
use Image;
use Intervention\Image\ImageManager;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class NegocioController extends Controller
{
    public function index()
    {   
        $dato = auth()->user()->id;
        $posts = Comerciodetalle::join('comercios', 'comerciodetalles.id_comercio','=','comercios.id')
        ->join('users', 'comercios.id_user','=','users.id')
        ->select('comercios.id as id'
                ,'comercios.nombre as nombre'
                ,'comercios.telefono as telefono'
                ,'comerciodetalles.id as id_detalle'
                ,'comerciodetalles.servicio1 as servicio1'
                ,'comerciodetalles.servicio2 as servicio2'
                ,'comerciodetalles.servicio3 as servicio3'
                ,'comerciodetalles.descripcion as descripcion'
                ,'comerciodetalles.contacto as contacto'
                ,'comerciodetalles.ruta as ruta'
                )->where('users.id','=', $dato)->get();
             return view('negocios.index', compact('posts'));        
    }

    public function show(Request $request, $id)
    {       
        $posts = Comerciodetalle::join('comercios', 'comerciodetalles.id_comercio','=','comercios.id')
        ->join('users', 'comercios.id_user','=','users.id')
        ->select('comercios.id as id'
                ,'comercios.nombre as nombre'
                ,'comercios.telefono as telefono'
                ,'comerciodetalles.id as id_detalle'
                ,'comerciodetalles.servicio1 as servicio1'
                ,'comerciodetalles.servicio2 as servicio2'
                ,'comerciodetalles.servicio3 as servicio3'
                ,'comerciodetalles.descripcion as descripcion'
                ,'comerciodetalles.contacto as contacto'
                , 'users.name as name'
                )->where('comercios.id','=', $id)->first();         
              return view('negocios.show', compact('posts'));                 
    }

    public function edit(Request $request, $id)
    {       
        $negocio = Comerciodetalle::find($id);
        return view('negocios.edit', compact('negocio'));    
    }


    public function update(NegocioRequest $request, $id)
    {
      Comerciodetalle::find($id)->update($request->all());
     return redirect()->route('negocios.index');
    }


    public function file(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'file'=> 'required |image|mimes:jpg,jpeg,png|max:5120',
        ]);
      
        if($validator -> fails()){
            return back()
            ->withInput()
            ->with('errorIsert','Favor subir archivo correcto')
            ->withErrors($validator);
        }
        else{  
                      
            $imagen = $request->file('file');
            $nombre = time().'.'.$imagen-> getClientOriginalExtension();
            $destino = public_path('img/negocios');
            // $request ->file->move($destino , $nombre);

            $red = Image::make($request->file('file'));
            $red->resize(600,366, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->rotate(-0);    
            $red->save($destino.'/'.$nombre);
            
             $dato = auth()->user()->id;
            $posts = Comerciodetalle::join('comercios', 'comerciodetalles.id_comercio','=','comercios.id')
            ->join('users', 'comercios.id_user','=','users.id')
            ->select('comerciodetalles.id as id_detalle'     
            )->where('users.id','=', $dato)->first();
            
            $id = $posts -> id_detalle;        
            
            $negocio = Comerciodetalle::find($id); 
            $deleteImg = '../public/img/negocios/'.$negocio->ruta;
            if(file_exists($deleteImg)) {
                File::delete($deleteImg);
            }
            $negocio->ruta = $nombre;
            $negocio->save();
           return redirect()->back()->with('success', 'Foto Actualizada');             
            // dd($posts);         
        }
        // return $request->file('file');        
    }
}

