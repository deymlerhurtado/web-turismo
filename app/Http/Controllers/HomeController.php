<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Encuesta;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {            
        return view('home');
    }

    public function grafica(){
        $datas = DB::select('select tp.respuesta , (select count(*) from encuestas p where p.id_votos = tp.id)
        as total from votos tp;');
        return response()->json($datas);
    }
    // $pedidos = DB::select('select tp.nombre , (select count(*) from pedidos p where p.id_tipo_pedidos = tp.id and p.id_estado_pedido = 2)
    //     as total from tipo_pedidos tp ;');
}
