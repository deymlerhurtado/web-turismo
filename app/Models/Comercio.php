<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{
    protected $fillable = [
        'nombre',
        'telefono',
        'estado',
        'id_estado',
        'id_user'
    ];
    use HasFactory;
}
