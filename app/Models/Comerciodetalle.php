<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comerciodetalle extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id_comercio',
        'servicio1',
        'servicio2',
        'servicio3',
        'descripcion',
        'contacto',
        'ruta'
    ];
    use HasFactory;
}
