@include('navbar')
<br>
 <!-- Breadcrumbs -->
 <section class="breadcrumbs-custom-inset">
        <div class="breadcrumbs-custom context-dark bg-overlay-60">
          <div class="container">
            <h2 class="breadcrumbs-custom-title">A cerca de Tilapa</h2>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.html">ir al inicio</a></li>           
            </ul>
          </div>
          <div class="box-position" style="background-image: url(img/acerca/img-1.jpg);"></div>
        </div>
      </section>
 <!-- Discover New Horizons-->
 <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
          <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
            <div class="col-lg-6 text-center wow fadeInUp"><img src="img/acerca/img-2.jpg" alt="" width="556" height="382"/>
            </div>
            <div class="col-lg-6 wow fadeInRight" data-wow-delay=".1s">
              <div class="box-width-lg-470">
                <h3>Descubre nuevos paisajes</h3>
                <!-- Bootstrap tabs-->
                <div class="tabs-custom tabs-horizontal tabs-line tabs-line-big tabs-line-style-2 text-center text-md-left" id="tabs-7">
                  <!-- Nav tabs-->
                  <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-7-1" data-toggle="tab">Tilapa</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-7-2" data-toggle="tab">Mision</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-7-3" data-toggle="tab">Vision</a></li>
                  </ul>
                  <!-- Tab panes-->
                  <div class="tab-content">
                    <div class="tab-pane fade show active" id="tabs-7-1">
                      <p>La Playa de Tilapa o también llamada isla Tular se encuentra en el sur occcidente del país, situada en el municipio de la Blanca, Departamento de San Marcos.
                          esta bella playa cuenta con vista al océano pacífico, su comercio de deriva de los servicios turísticos, de recreación familiar, la pesca y venta de mariscos.
                      </p>
                    </div>
                    <div class="tab-pane fade" id="tabs-7-2">
                      <p>Ofrecer excelente calidad y precios accesibles en nuestros comercios que le brindan la oportunidad de experimentar el destino turistico elegido de una manera auténtica y emocionante.</p>
                    </div>
                    <div class="tab-pane fade" id="tabs-7-3">
                      <p>Brindar la mejor experiencia de turismo y de recreación familiar al mismo tiempo que se fomenta el comercio para el desarrollo del área turística en la playa de Tilapa en el municipio de La Blanca, San Marcos.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<!-- destinos -->
      <section class="section section-sm section-fluid bg-default">
        <div class="container">
          <h3 style="text-align: center;">Paisajes</h3>
        </div>
        <!-- Owl Carousel-->
        <div class="owl-carousel owl-classic owl-timeline" data-items="1" data-md-items="2" data-lg-items="3" data-xl-items="4" data-margin="30" data-autoplay="false" data-nav="true" data-dots="true">
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-3.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-3.jpg" data-lightgallery="item"></a>
              </div>
            </article>            
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-4.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-4.jpg" data-lightgallery="item"></a>
              </div>
            </article>            
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-5.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-5.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-6.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-6.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-7.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-7.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-8.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-8.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-9.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-9.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
          <div class="owl-item">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/acerca/min/img-10.jpg" alt="" width="420" height="308"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/acerca/img-10.jpg" data-lightgallery="item"></a>
              </div>
            </article>           
          </div>
        </div>
      </section>
      
         <!-- What people Say-->
      <section class="section section-sm section-last bg-default">
        <div class="container">
          <h3 style="text-align: center;">Antecedentes</h3>
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-modern" data-items="1" data-stage-padding="15" data-margin="30" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" data-autoplay="false">
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body">
                <div class="">
                <p class="q">Antes de alcanzar el nivel de Municipio, La Blanca inició perteneciendo como una aldea del municipio de Ocós. 
                    La comunidad conformada de vecinos solicitó convertirse en municipio al honorable Congreso de la República de Guatemala siendo aprobada la solicitud el 23 de enero del año 2014. 
                    Después de seis años de que los vecinos iniciaron los trámites de deslinde en la GobernaciónLa Blanca se convierte en el municipio número 30 del departamento de San Marcos y en el no. 335 de Guatemala.</p>
              </div>
                <h5 class="quote-lisa-cite">(GRAMAJO, ÁLVAREZ, & COYOY, 2014).</h5>
                <p class="quote-lisa-status"><a href="https://www.prensalibre.com/guatemala/comunitario/blanca-convirtio-ayer-municipio-0-1072092805/" target="_blank">Para Prensa Libre</a></p>
              </div>
            </article>
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body">
                <div class="">
                  <p class="q">El municipio de La Blanca se encuentra ubicado en la parte Sur del departamento de San Marcos. Limita al norte con el municipio de Coatepeque del departamento de Quetzaltenango, 
                    al Este con el municipio de Retalhuleu, al Oeste con los municipios de Ayutla y Ocós; al Sur con el Océano Pacífico. </p>
                </div>
                <h5 class="quote-lisa-cite"><a href="#">La Blanca (San Marcos)</h5>
                <p class="quote-lisa-status"><a href="https://es.wikipedia.org/wiki/La_Blanca_(San_Marcos)"  target="_blank">wikipedia, enciclopedia libre</a></p>
              </div>
            </article>
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body">
                <div class="">
                  <p class="q">La actividad turística se realiza en la Playa de Tilapa, un lugar con hermosas vistas hacia el océano pacifico, lleno de tranquilidad y con un ornato adecuado para el visitante. Se puede dormir en apartamentos privados y comer en pequeños restaurantes. En esta playa también se ubica una pequeña extensión de la reserva natural declarada por CONAP denominada Manchón Guamuchal,
                     en donde el visitante puede apreciar uno de los siete Humedales Ramsar en nuestro país.</p>
                </div>
                <h5 class="quote-lisa-cite">INGUAT (2019)</h5>
                <p class="quote-lisa-status"><a href="https://inguat.gob.gt/index.php/gestion-turistica/planes-de-desarrollo-turistico" target="_blank">Instituto Guatemalteco de Turismo</a></p>
              </div>
            </article>
          </div>
        </div>
      </section>
          <!-- Counter Classic-->
          <section class="section section-fluid bg-default">
        <div class="parallax-container" data-parallax-img="img/acerca/img-11.jpg">
          <div class="parallax-content section-xl context-dark bg-overlay-26">
            <div class="container">
              <div class="row row-50 justify-content-center border-classic">
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">8</span>
                    </div>
                    <h5 class="counter-classic-title">Restaurantes</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">5</span>
                    </div>
                    <h5 class="counter-classic-title">Piscinas</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">3</span>
                    <!-- <span class="symbol">5</span> -->
                    </div>
                    <h5 class="counter-classic-title">Hoteles</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">700</span>
                    </div>
                    <h5 class="counter-classic-title">Turistas</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <br>
@include('footer')