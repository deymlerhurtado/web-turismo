@include('navbar')
<br>
 <!-- Hot tours-->
 <section class="section section-sm bg-default">
        <div class="container">
          <h3 class="oh-desktop"><span class="d-inline-block wow slideInDown">Negocios en Tilapa</span></h3>
          <div class="row row-sm row-40 row-md-50">
            <div class="col-sm-6 col-md-12 wow fadeInRight">
              <!-- Product Big-->
              @foreach($negocios as $negocio)            
              <article class="product-big">
                <div class="unit flex-column flex-md-row align-items-md-stretch">
                  <div class="unit-left"><a class="product-big-figure" href=""><img src="img/negocios/{{$negocio->ruta}}" alt="" width="600" height="366"/></a></div>
                  <div class="unit-body">
                    <div class="product-big-body">
                      <h5 class="product-big-title"><a href="">{{$negocio->nombre}}</a></h5>
                      <div class="group-sm group-middle justify-content-start">
                        <div class="product-big-rating">Servicios</div>                    
                      </div>
                        <h6 class="product-big-text"><a href="#">{{$var=1}} {{$negocio->servicio1}}</a></h6>
                        <h6 class="product-big-text"><a href="#">{{$var+1}} {{$negocio->servicio2}}</a></h6>
                        <h6 class="product-big-text"><a href="#">{{$var+2}} {{$negocio->servicio3}}</a></h6>                     
                      <div class="group-sm group-middle justify-content-start">
                        <div class="product-big-rating">Descripción</div>
                      </div>
                      <h6 class="product-big-text">{{$negocio->descripcion}}</h6>                       
                      <div class="group-sm group-middle justify-content-start">
                        <div class="product-big-rating">Ubicación</div>
                        <p class="product-big-text">Playa de Tilapa, La Blanca, San Marcos.</p>  <!-- <a class="button button-black-outline button-ujarak" href="#">Buy This Tour</a> -->
                        </div>                                               
                        <div class="product-big-price-wrap"><span class="product-big-price">Tel. {{$negocio->contacto}}</span></div>
                    </div>
                  </div>
                </div>
              </article>
              <br><br>
              @endforeach

              {{$negocios->links()}}
            </div>         
          </div>
        </div>
      </section>
      @include('footer')