@include('navbar')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN9lLw1TfKGKcHFsFNanDZ-GQwjoIIJ64&callback=initMap&libraries=&v=weekly" async></script>
<style>#map{width:100%;height: 400px;}</style>
<br>
<br>
<br>
<h3 style=" text-align: center">Ubicación Geografica</h3>
<br>
<div id="map">
</div>
      <!-- Contact information-->
      <section class="section section-sm section-first bg-default">
        <div class="container">
          <div class="row row-30 justify-content-center">
            <div class="col-sm-8 col-md-6 col-lg-4">
              <article class="box-contacts">
                <div class="box-contacts-body">
                  <div class="box-contacts-icon fl-bigmug-line-map87"></div>
                  <div class="box-contacts-decor"></div>
                  <p class="box-contacts-link">Para llegar se conduce por la carretera departamental tilapa-pajapita hacia el municipio de la Blanca San Marcos.</p>
                
                </div>
              </article>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
              <article class="box-contacts">
                <div class="box-contacts-body">
                  <div class="box-contacts-icon fl-bigmug-line-weather21"></div>
                  <div class="box-contacts-decor"></div>
                  <p class="box-contacts-link">Este lugar túristico se encuentra en el sur occcidente del pais, en el municipio de la Blanca departamento de San Marcos.</p>
                </div>
              </article>
            </div>
            
            </div>
        </div>
      </section>
@include('footer')