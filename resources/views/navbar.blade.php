<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <title>Tilapa  | El Tular</title>       
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- styles -->
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7CPoppins:400%7CTeko:300,400">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/fonts.css">
         <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
         <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- Scripts -->
        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        
          <!-- token form-encuesta -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-corporate" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="106px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
            <div class="rd-navbar-aside-outer">
              <div class="rd-navbar-aside">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">                
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand">
                    <a class="brand" href="/"><img src="img/logo.png" alt=""  width="225" height="18"/></a>
                  </div>
                </div>
                <div class="rd-navbar-aside-right rd-navbar-collapse">
                  <ul class="rd-navbar-corporate-contacts">
                   <li><a class="button button-md button-default-outline-2 button-ujarak" href="{{route('virtual')}}">Recorrido Virtual</a></li>
                    <li>
                      <div class="unit unit-spacing-xs">
                      <div class="">
                            @if (Route::has('login'))
                            <div class="">
                               @auth
                                    <a href="{{ url('/home') }}" class="button button-md button-default-outline-2 button-ujarak">Home</a>
                             @else
                                     <a href="{{ route('login') }}" class="button button-md button-default-outline-2 button-ujarak">Login</a>
                               @endif
                              </div>
                              @endif       
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <div class="rd-navbar-nav-wrap">
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li class="rd-nav-item"><a class="rd-nav-link" href="/">Inicio</a>
                    </li>
                    <li class="rd-nav-item"><a class="rd-nav-link" href="{{route('negocios')}}">Negocios</a>
                    </li>
                    <li class="rd-nav-item"><a class="rd-nav-link" href="{{route('about')}}">Acerca de</a>
                    </li>
                    <li class="rd-nav-item"><a class="rd-nav-link" href="{{route('ubicacion')}}">Ubicacion</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>