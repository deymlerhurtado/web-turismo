<footer class="footer">
    <div class="container">
        <nav class="float-left">
            <ul>
                <!-- <li>
                    <a href="https://www.creative-tim.com">
                        {{ __('Creative Tim') }}
                    </a>
                </li>
                <li>
                    <a href="https://creative-tim.com/presentation">
                        {{ __('About Us') }}
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com">
                        {{ __('Blog') }}
                    </a>
                </li>
                <li>
                    <a href="https://www.creative-tim.com/license">
                        {{ __('Licenses') }}
                    </a>
                </li> -->
            </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, La Blanca <i class="material-icons">favorite</i> 
        <!-- <a href="https://www.creative-tim.com" target="_blank">Turismo Tilapa</a> -->
        </div>
    </div>
</footer>