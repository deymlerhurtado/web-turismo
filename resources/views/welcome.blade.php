@include('navbar')
<!-- cuerpo -->
<section class="section swiper-container swiper-slider swiper-slider-corporate swiper-pagination-style-2" data-loop="true" data-autoplay="5000" data-simulate-touch="true" data-nav="false" data-direction="vertical">
        <div class="swiper-wrapper text-left">
          <div class="swiper-slide context-dark" data-slide-bg="/img/inicio/tour-1.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <div class="row">
                  <div class="col-md-10">
                    <h6 class="text-uppercase" data-caption-animate="fadeInRight" data-caption-delay="0">Disfruta de los mejores destinos</h6>
                    <h2 class="oh font-weight-light" data-caption-animate="slideInUp" data-caption-delay="100"><span>Viaja</span><span class="font-weight-bold"> a la playa de Tilapa</span></h2>
                    <!-- <a class="button button-default-outline button-ujarak" href="#" data-caption-animate="fadeInLeft" data-caption-delay="0">Get in touch</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide context-dark" data-slide-bg="/img/inicio/tour-2.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <div class="row">
                  <div class="col-md-10">
                    <h6 class="text-uppercase" data-caption-animate="fadeInRight" data-caption-delay="0">El lugar más calido</h6>
                    <h2 class="oh font-weight-light" data-caption-animate="slideInUp" data-caption-delay="100"><span>Difruta</span><span class="font-weight-bold"> del clima</span></h2>
                    <!-- <a class="button button-default-outline button-ujarak" href="#" data-caption-animate="fadeInLeft" data-caption-delay="0">Get in touch</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide context-dark" data-slide-bg="/img/inicio/tour-3.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <div class="row">
                  <div class="col-md-10">
                    <h6 class="text-uppercase" data-caption-animate="fadeInRight" data-caption-delay="0">Construye tu próximo viaje</h6>
                    <h2 class="oh font-weight-light" data-caption-animate="slideInUp" data-caption-delay="100"><span>Crea</span><span class="font-weight-bold"> tu destino</span></h2>
                    <!-- <a class="button button-default-outline button-ujarak" href="#" data-caption-animate="fadeInLeft" data-caption-delay="0">Get in touch</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Swiper Pagination-->
        <div class="swiper-pagination"></div>
      </section>
      <!-- Section Box Categories-->
      <section class="section section-lg section-top-1 bg-gray-4">
        <div class="container offset-negative-1">
          <div class="box-categories cta-box-wrap">
            <div class="box-categories-content">
              <div class="row justify-content-center">
                <div class="col-md-4 wow fadeInDown col-9" data-wow-delay=".2s">
                  <ul class="list-marked-2 box-categories-list">
                    <li><a href="#"><img src="img/inicio/img-1.jpg" alt="" width="368" height="420"/></a>
                      <h5 class="box-categories-title">Paseo en Lancha</h5>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 wow fadeInDown col-9" data-wow-delay=".2s">
                  <ul class="list-marked-2 box-categories-list">
                    <li><img src="img/inicio/img-2.jpeg" alt="" width="368" height="420"/>
                      <h5 class="box-categories-title">Vacaciones la playa</h5>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 wow fadeInDown col-9" data-wow-delay=".2s">
                  <ul class="list-marked-2 box-categories-list">
                    <li><img src="img/inicio/img-3.jpg" alt="" width="368" height="420"/>
                      <h5 class="box-categories-title">Amplio Parqueo</h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>    
        </div>
      </section>
<!-- end -->
      <!--	Our Services-->
      <section class="section section-sm">
        <div class="container">
        <h3 style="text-align: center;">Servicios</h3>
          <div class="row row-30">
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-equalization3"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Parqueos</h5>
                    <p class="box-icon-classic-text">confianza en el parqueo para su vehiculo, por horas o dias.</p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-circular220"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Seguridad</h5>
                    <p class="box-icon-classic-text">Brindando seguridad para todos nuestros turistas.</p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-favourites5"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Atención y Servicio</h5>
                    <p class="box-icon-classic-text">Personas capacitadas y calificadas para brindar un servicio amable y atento.</p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-like51"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Bioseguridad</h5>
                    <p class="box-icon-classic-text">Todos los comercios cuentan con  medidas de bioseguridad.</p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-hot67"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Restaurantes</h5>
                    <p class="box-icon-classic-text">Calidad en la gastronomía de los restaurantes.</p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article class="box-icon-classic">
                <div class="unit box-icon-classic-body flex-column flex-md-row text-md-left flex-lg-column text-lg-center flex-xl-row text-xl-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon fl-bigmug-line-store10"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">Salud</h5>
                    <p class="box-icon-classic-text">Farmacias atentiendo 365 dias del año.</p>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>      
           <!-- Why choose us-->
           <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
          <div class="row row-50 justify-content-center align-items-xl-center">
            <div class="col-md-10 col-lg-5 col-xl-6"><img src="img/inicio/img-4.jpg" alt="" width="519" height="564"/>
            </div>
            <div class="col-md-10 col-lg-7 col-xl-6">
              <h1 class="text-spacing-25 font-weight-normal title-opacity-6">Por qué elegir Tilapa</h1>
              <!-- Bootstrap tabs-->
              <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-4">
                <!-- Nav tabs-->
                <ul class="nav nav-tabs">
                  <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-4-1" data-toggle="tab">Servicios</a></li>
                  <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-4-2" data-toggle="tab">Negocios</a></li>
                  <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-4-3" data-toggle="tab">Recreacion</a></li>
                </ul>
                <!-- Tab panes-->
                <div class="tab-content">
                  <div class="tab-pane fade show active" id="tabs-4-1">
                    <p>En la playa de Tilapa los comercios tienen años de trayectoria brindando excelente servicio a los turistas y locales .</p>
                    <!-- Linear progress bar-->
                    <article class="progress-linear progress-secondary">
                      <div class="progress-header">
                        <p>Agilidad</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear" data-gradient=""><span class="progress-value">100</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                    <!-- Linear progress bar-->
                    <article class="progress-linear progress-orange">
                      <div class="progress-header">
                        <p>Amabilidad</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear" data-gradient=""><span class="progress-value">100</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                    <!-- Linear progress bar-->
                    <article class="progress-linear">
                      <div class="progress-header">
                        <p>Experiencia</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear" data-gradient=""><span class="progress-value">100</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                  </div>
                  <div class="tab-pane fade" id="tabs-4-2">
                    <div class="row row-40 justify-content-center text-center inset-top-10">
                      <p></p>
                      <div class="col-sm-4">
                        <!-- Circle Progress Bar-->
                        <div class="progress-bar-circle" data-value="1.00" data-gradient="#01b3a7" data-empty-fill="transparent" data-size="150" data-thickness="12" data-reverse="true"><span></span></div>
                        <p class="progress-bar-circle-title">Calidad</p>
                      </div>
                      <div class="col-sm-4">
                        <!-- Circle Progress Bar-->
                        <div class="progress-bar-circle" data-value="1.00" data-gradient="#01b3a7" data-empty-fill="transparent" data-size="150" data-thickness="12" data-reverse="true"><span></span></div>
                        <p class="progress-bar-circle-title">Bioseguridad</p>
                      </div>
                      <div class="col-sm-4">
                        <!-- Circle Progress Bar-->
                        <div class="progress-bar-circle" data-value="1.00" data-gradient="#01b3a7" data-empty-fill="transparent" data-size="150" data-thickness="12" data-reverse="true"><span></span></div>
                        <p class="progress-bar-circle-title">Confiabilidad</p>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tabs-4-3">
                    <p>Para la recreacion familiar exiten diferentes actividades que puede elegir</p>
                    <div class="text-center text-sm-left offset-top-30 tab-height">
                      <ul class="row-16 list-0 list-custom list-marked list-marked-sm list-marked-secondary">
                        <li>Paseo en lancha mar adentro (semana mayor o fin de año)</li>
                        <li>Paseo en lancha por el manglar (siempre)</li>
                        <li>Paseo en cuatrimoto (semana mayor o fin de año)</li>
                        <li>conocer las bocabarrras (siempre)</li>
                        <li>Acampar en la playa (siempre)</li>
                        <li>Caminatas por la arena (siempre)</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>  
    <!--	Instagrram wondertour-->
           <section class="section section-sm section-top-0 section-fluid section-relative bg-gray-4">
        <div class="container-fluid">
           <h3 style="text-align: center;">Galeria</h3>
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-classic owl-dots-secondary" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="4" data-xl-items="5" data-xxl-items="6" data-stage-padding="15" data-xxl-stage-padding="0" data-margin="30" data-autoplay="true" data-nav="true" data-dots="true">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-1.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-1.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-2.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-2.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-3.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-3.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-4.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-4.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-5.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-5.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-6.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-6.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-7.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-7.jpg" data-lightgallery="item"></a>
              </div>
            </article>
             <!-- Thumbnail Classic-->
             <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-8.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-8.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-9.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-9.jpg" data-lightgallery="item"></a>
              </div>
            </article>
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-mary">
              <div class="thumbnail-mary-figure"><img src="img/inicio/min/gal-10.jpg" alt="" width="270" height="195"/>
              </div>
              <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="img/inicio/gal-10.jpg" data-lightgallery="item"></a>
              </div>
            </article>
          </div>
        </div>
      </section>
      <!-- votacion --> 
       <!-- Contact information-->
       <div id="seccion">
         <section class="section section-sm section-first bg-default" >
           <h4 style="text-align: center;">Lo invitamos ha responder esta encuesta</h4>
           <br>
           <div class="container">
             <div class="row row-30 justify-content-center">
               <div class="col-sm-8 col-md-6 col-lg-4">
              <article class="box-contacts">
                <div class="box-contacts-body">
                  <h5 class="box-icon-classic-title">Has visitado la playa de Tilapa. ¿Qué tal te parece este lugar turístico?</h5>
                  <form method="POST" action="" id="form-encuesta" class="form-horizontal">
                    @csrf   
                    <br>
                      <div class="form-group" class="col-lg-4">
                        <input type="radio" name="id_votos"  value="1"> <label class="box-icon-classic-title">Excelente</label><br>
                      </div>
                      <div class="form-group" class="col-lg-4">
                        <input type="radio" name="id_votos" value="2"> <label class="box-icon-classic-title">Bueno</label><br> 
                      </div>
                      <div class="form-group">
                      <input type="radio" name="id_votos"  value="3"> <label class="box-icon-classic-title">Regular</label><br>
                    </div>
                      <div class="form-group">
                        <input type="radio" name="id_votos"  value="4"> <label class="box-icon-classic-title">Hay que mejorar</label><br>                      
                      <input type="button" class="btn btn-primary" id="encuesta" value="Enviar">
                    </div>
                    <br>
                  </form>             
                </div>
              </article>
            </div>     
          </div>
        </div>
      </section>
    </div>
     <!-- Contact information-->
     <div id="retorno">
     <section class="section section-sm section-first bg-default">
     <h4 style="text-align: center;">Gracias por su participación</h4>
           <br>
        <div class="container">
          <div class="row row-30 justify-content-center">
            <div class="col-sm-8 col-md-6 col-lg-4">
              <article class="box-contacts">
                <div class="box-contacts-body">
                  <div class="box-contacts-icon fl-bigmug-line-checkmark15"></div>
                  <div class="box-contacts-decor"></div>
                  <p class="box-contacts-link">Su respuesta se ha enviado correctamente, gracias.</p>            
                </div>
              </article>
            </div>          
          </div>
        </div>
      </section>
    </div>
      <!-- Mision Vision -->
<section class="section bg-default text-center offset-top-50">
        <div class="parallax-container" data-parallax-img="img/inicio/img-5.jpg">
          <div class="parallax-content section-xl section-inset-custom-1 context-dark bg-overlay-2-21">
            <div class="container">
              <h2 class="heading-2 oh font-weight-normal wow slideInDown"><span class="d-block font-weight-semi-bold">La Playa de Tilapa</span><span class="d-block font-weight-light">!esta esperandote!</span></h2>
              <p class="text-width-medium text-spacing-75 wow fadeInLeft" data-wow-delay=".1s">¡Navegue por nuestro sitio web y encontrará el tour de sus sueños!</p>
            </div>
          </div>
        </div>
</section>
<br>
@include('footer')