<!-- Footer -->
<footer class="section footer-corporate context-dark">       
        <div class="footer-corporate-bottom-panel">
          <div class="container">
            <div class="row justfy-content-xl-space-berween row-10 align-items-md-center2">
              <!-- <div class="col-sm-6 col-md-4 text-sm-right text-md-center"> 
                <div>
                  <ul class="list-inline list-inline-sm footer-social-list-2">
                    <li><a class="icon fa fa-facebook" href="#"></a></li>
                    <li><a class="icon fa fa-twitter" href="#"></a></li>
                    <li><a class="icon fa fa-google-plus" href="#"></a></li>
                    <li><a class="icon fa fa-instagram" href="#"></a></li>
                  </ul>
                </div>
              </div> -->     
                <!-- Rights-->
              </div>
              <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>Playa de Tilapa</span>. Todos los derechos reservados.</p>           
            </div>
          </div>
        </div>
      </footer>
           <!-- Javascript-->     
           <script src="js/jquery.min.js"></script>
           <script src="js/core.min.js"></script>
           <script src="js/script.js"></script>
           <script src="/js/localizacion.js"></script>
           <script src="/js/main.js"></script>
           <script src="js/encuesta.js"></script>
       
    </body>
</html>
