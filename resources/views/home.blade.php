@extends('layouts.main', ['activePage' => 'dashboard', 'titlePage' => __('Home')])
@section('content')
<div class="content">
    <div class="container-fluid">
         <div style="text-align:center" class="card mb-5 shadow-sm border-0 shadow-hover">
        <h2>Bienvedidos</h2>
        <h3>Plataforma web y recorrido virtual de la playa de Tilapa</h3>
         </div>
    </div>
    <div class="row">
    <div class="col-xl-6 col-lg-6">
        <div class="card mb-5 shadow-sm border-0 shadow-hover">
            <div class="row">
                <div class="col col-12">
                    <h4 class="text-center mt-2">Conteo de encuesta</4>
                    <h5>Has visitado la playa de Tilapa. ¿Qué tal te parece este lugar turístico?</h5>
                </div>
            </div>
            <div class="card-body d-flex">
                <canvas id="barChart"></canvas>
            </div>
        </div>
    </div>
    </div>  
</div>
    <!-- <div class="container-fluid">
        <div style="text-align:center" class="card mb-5 shadow-sm border-0 shadow-hover">
        <h2>Conteo de la encuesta</h2>
            <div style="height:400px; width:700px; margin:auto;"> 
            <canvas id="barChart"></canvas>
            </div>
        </div>
    </div>
    <br><br> -->
<script src="{{ asset('js/grafica.js') }}" defer></script>
@endsection