@extends('layouts.main', ['activePage' => 'language', 'titlePage' => 'Detalles del Comercio'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <!--Header-->
          <div class="card-header card-header-primary">
            <h4 class="card-title">Información</h4>
            <p class="card-category">Vista detallada comercio: {{$comercio -> nombre }} </p>
          </div>
          <!--End header-->
          <!--Body-->
          <div class="card-body">
            <div class="row">
              <!-- first -->
              <div class="col-md-4">
                <div class="card card-user">
                  <div class="card-body">
                    <p class="card-text">
                      <div class="author">
                        <div class="block block-one"></div>
                        <div class="block block-two"></div>
                        <div class="block block-three"></div>
                        <div class="block block-four"></div>
                          <img class="avatar" src="{{ asset('/img/default-avatar.png') }}" alt="">
                          <h5 class="title mt-3">Nombre Comercio: {{$comercio -> nombre}}</h5>                    
                        <p class="description">    
                        <h5 class="title mt-3">Número Telefono: {{$comercio -> telefono}}</h5>              
                        </p>
                     
                        <p class="description">    
                        @if($comercio->id_estado === 1)
                        <h5 class="title mt-3">Estado: Activo</h5> 
                       @endif
                        @if($comercio->id_estado === 2)
                       <h5 class="title mt-3">Estado: Inactivo</h5> 
                          @endif
                                     
                        </p>
                      </div>
                    </p>
                    <div class="card-description">
                  
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="button-container">
                    <a href="{{ route('comercios.edit', $comercio ->id) }}" class="btn btn-success"> <i
                          class="material-icons">edit</i> </a>
                    </div>
                  </div>
                </div>
              </div>
              <!--end first-->
            </div>
            <!--end row-->
          </div>
          <!--End card body-->
        </div>
        <!--End card-->
      </div>
    </div>
  </div>
</div>
@endsection