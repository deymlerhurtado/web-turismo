@extends('layouts.main', ['activePage' => 'language', 'titlePage' => 'Editar Comercio'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="POST" action="{{ route('comercios.update', $comercio->id) }}" class="form-horizontal">
          @csrf
          @method('PUT')
          <div class="card">
            <!--Header-->
            <div class="card-header card-header-primary">
              <h4 class="card-title">Editar Comercio</h4>
              <p class="card-category">Editar datos del Comercio</p>
            </div>
            <!--End header-->
            <!--Body-->
            <div class="card-body">
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="nombre" placeholder="Ingrese el nombre"
                    value="{{ old('nombre', $comercio->nombre) }}" autocomplete="off" autofocus>
                    @if ($errors->has('nombre'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('nombre') }}</span>
                  @endif
                </div>
              </div>
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Telefono de contacto</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="telefono" placeholder="Ingrese el telefono"
                    value="{{ old('telefono', $comercio->telefono) }}" autocomplete="off" autofocus>
                    @if ($errors->has('telefono'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('telefono') }}</span>
                  @endif
                </div>
              </div>

              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Estado del comercio</label>
                <div class="col-sm-7">
                <input type="radio" name="id_estado" value="1"  {{ ($comercio->id_estado==1)? "checked" : "" }} ><label for="">Activo</label><br>
                <input type="radio" name="id_estado" value="2"  {{ ($comercio->id_estado==2)? "checked" : "" }} ><label for="">Inactivo</label>
                </div>
              </div>

             
              
            </div>
            <!--End body-->
            <!--Footer-->
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <!--End footer-->
        </form>
      </div>
    </div>
  </div>
</div>
@endsection