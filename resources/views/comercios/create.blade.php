@extends('layouts.main', ['activePage' => 'Comercio', 'titlePage' => 'Nuevo Comercio'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="POST" action="{{ route('comercios.store') }}" class="form-horizontal">
          @csrf
          <div class="card ">
            <!--Header-->
            <div class="card-header card-header-primary">
              <h4 class="card-title">Nuevo Comercio</h4>
              <p class="card-category">Ingresar </p>
            </div>
            <!--End header-->
            <!--Body-->
            <div class="card-body">
            {{-- @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
              @endif --}}
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Nombre de Comercio</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="nombre" placeholder="Ingrese el nombre" value="{{ old('nombre') }}"
                    autocomplete="off" autofocus>
                    @if ($errors->has('nombre'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('nombre') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Número telefonico</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="telefono" placeholder="Ingrese el número" value="{{ old('telefono') }}"
                    autocomplete="off" autofocus>
                    @if ($errors->has('telefono'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('telefono') }}</span>
                  @endif
                </div>
              </div>
            </div>

           
            
            <div class="card-body">
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Seleccionar Usuario</label>
                <div class="col-sm-7">         
                <select id="id_user"  class="custom-select" name="id_user" required>
                <option value="" selected hidden>Elegir</option>
                      @foreach($users as $user)
                      <option value="{{$user -> id}}">{{ $user -> name }}</option>
                      @endforeach
                    </select>
                </div>
              </div>
            </div>

            <!--End body-->

            <!--Footer-->
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            <!--End footer-->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection