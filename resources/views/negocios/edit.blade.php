@extends('layouts.main', ['activePage' => 'language', 'titlePage' => 'Editar Negocio'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="POST" action="{{ route('negocios.update', $negocio->id) }}" class="form-horizontal">
          @csrf
          @method('PUT')
          <div class="card">
            <!--Header-->
            <div class="card-header card-header-primary">
              <h4 class="card-title">Editar Detalles de mi negocio</h4>
              <p class="card-category">Editar</p>
            </div>
            <!--End header-->
            <!--Body-->
            <div class="card-body">
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Servicio 1</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="servicio1" placeholder="Ingrese Servicio 1"
                    value="{{ old('servicio1', $negocio->servicio1) }}" autocomplete="off" autofocus>
                    @if ($errors->has('servicio1'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('servicio1') }}</span>
                    @endif
                  </div>
              </div>

              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Servicio 2</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="servicio2" placeholder="Ingrese Servicio 2"
                    value="{{ old('servicio2', $negocio->servicio2) }}" autocomplete="off" autofocus>
                    @if ($errors->has('servicio2'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('servicio2') }}</span>
                    @endif
                </div>
              </div>

              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Servicio 3</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="servicio3" placeholder="Ingrese Servicio 3"
                    value="{{ old('servicio3', $negocio->servicio3) }}" autocomplete="off" autofocus>
                    @if ($errors->has('servicio3'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('servicio3') }}</span>
                    @endif
                </div>
              </div>

              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Descripción</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="descripcion" placeholder="Descripcion de negocio"
                    value="{{ old('descripcion', $negocio->descripcion) }}" autocomplete="off" autofocus>
                    @if ($errors->has('descripcion'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('descripcion') }}</span>
                    @endif
                </div>
              </div>
              <div class="row">
                <label for="title" class="col-sm-2 col-form-label">Contacto Publico</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="contacto" placeholder="Opcional"
                    value="{{ old('contacto', $negocio->contacto) }}" autocomplete="off" autofocus>
                    @if ($errors->has('contacto'))
                    <span class="error text-danger" for="input-name">{{ $errors->first('contacto') }}</span>
                    @endif
                </div>
              </div>

            </div>
            <!--End body-->
            <!--Footer-->
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <!--End footer-->
        </form>
      </div>
    </div>
  </div>
</div>
@endsection