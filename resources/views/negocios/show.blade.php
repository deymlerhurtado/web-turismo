@extends('layouts.main', ['activePage' => 'language', 'titlePage' => 'Detalles de Negocio'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <!--Header-->
          <div class="card-header card-header-primary">
            <h4 class="card-title">Mi Negocio</h4>
            <p class="card-category">Vista detallada de {{ $posts -> nombre }}</p>          
          </div>
          <!--End header-->
          <!--Body-->
          <div class="card-body">
            <div class="row">
              <!-- first -->
              <div class="col-md-4">
                <div class="card card-user">
                  <div class="card-body">
                    <p class="card-text">
                      <div class="author">
                        <div class="block block-one"></div>
                        <div class="block block-two"></div>
                        <div class="block block-three"></div>
                        <div class="block block-four"></div>
                        
                          <img class="avatar" src="{{ asset('/img/default-avatar.png') }}" alt="">
                          <h5 class="title mt-3"></h5>
                       
                        <p class="description">
                        <h5>Nombre Negocio: {{$posts -> nombre }}</h5><h5> Usuario Encargado: {{$posts -> name}}</h5>
                        </p>
                      </div>
                    </p>
                    <div class="description">
                    <h5>Servicio #1: {{$posts -> servicio1 }}</h5>
                    </div>
                    <div class="description">
                    <h5>Servicio #2: {{$posts -> servicio2 }}</h5>
                    </div>
                    <div class="description">
                    <h5>Servicio #3: {{$posts -> servicio3 }}</h5>
                    </div>
                    <div class="description">
                    <h5>descripción: {{$posts -> descripcion }}</h5>
                    </div>
                    <div class="description">
                    <h5>Contacto Publico: {{$posts -> contacto }}</h5>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="button-container">
                  
                      <a href="{{ route('negocios.edit', $posts->id_detalle) }}" class="btn btn-success"> <i
                          class="material-icons">edit</i> </a>
                 
                    </div>
                  </div>
                </div>
              </div>
              <!--end first-->
            </div>
            <!--end row-->
          </div>
          <!--End card body-->
        </div>
        <!--End card-->
      </div>
    </div>
  </div>
</div>
@endsection