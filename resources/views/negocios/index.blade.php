@extends('layouts.main', ['activePage' => 'language', 'titlePage' => 'Negocio'])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Mi negocio</h4><h4>Bienvenid@  {{ auth()->user()->name }} </h4>
            
            <p class="card-category">Lista de Detalles</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table ">
                <thead class="text-primary">
                  <th> ID </th>
                  <th> Nombre </th>
                  <th> Telefono</th>
                  <th> Servicio 1</th>
                  <th> Servicio 2</th>
                  <th> Servicio 3</th>
                  <th> Descripción</th>
                  <th> Contacto</th>
                  <th class="text-right"> Acciones </th>
                </thead>
                <tbody>
                  @foreach ($posts as $post)
                  <tr>
                  <td>{{ $post->id }}</td>
                    <td>{{ $post->nombre }}</td>
                    <td>{{ $post->telefono }}</td>
                    <td>{{ $post->servicio1 }}</td>    
                    <td>{{ $post->servicio2 }}</td>
                    <td>{{ $post->servicio3 }}</td>  
                    <td>{{ $post->descripcion }}</td>  
                    <td>{{ $post->contacto }}</td> 
                    <td class="td-actions text-right">
                    @can('user_boton')
                      <a href="{{ route('negocios.show', $post->id) }}" class="btn btn-info"> <i
                          class="material-icons">person</i> </a>
                    @endcan
                    @can('user_boton')
                      <a href="{{ route('negocios.edit', $post->id) }}" class="btn btn-success"> <i
                          class="material-icons">edit</i> </a>
                    @endcan
                    
                    </td> 
                  </tr>               
                  @endforeach
                </tbody>
              </table>
            </div>

            <div>
              <form action="{{route('negocios.file')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <h4>Foto de Portada</h4>
                <!-- <input type="hidden" name="id_detalle" id="id_detalle" value="{{$post->id_detalle}}" > -->
                <input type="file" name="file" id="file"   accept="image/*">
                @error('file')
                <small class="text-danger">{{$message}}</small>
                @enderror
                <button type="submit" class="btn btn-success"> <i
                          class="material-icons">add_a_photo</i></button>
               
              </form>
              @if (\Session::has('success'))
            <div class="alert alert-success">
              <ul>
              <li>{!! \Session::get('success') !!}</li>
          </ul>
           </div>
        @endif
            </div>
          </div>
          <!--Footer-->
          <div class="card-footer mr-auto">
          <div class="card">
            <!--Header-->
            <div class="card-header card-header-primary">
              <h4 class="card-title">Foto de Negocio</h4> 
            </div>
            <!--End header-->
            <!--Body-->
            <div class="card-body">
              <div class="row">
              @foreach ($posts as $post)            
               <!-- {{$img = $post->ruta}} -->
               <img src="img/negocios/{{$post->ruta}}" alt="" width="250px" height="auto">        
               @endforeach
               <!-- <img src="img/negocios/1631142781.png" alt="" width="250px" height="auto">           -->
      <!-- img/negocios/{{$post->ruta}} -->
              </div>
          </div>
          <!--End footer-->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection