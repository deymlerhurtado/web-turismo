<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/Negocios', [App\Http\Controllers\TuristaController::class, 'negocios'])->name('negocios');
Route::get('/Ubicacion', [App\Http\Controllers\TuristaController::class, 'ubicacion'])->name('ubicacion');
Route::get('/about', [App\Http\Controllers\TuristaController::class, 'about'])->name('about');
Route::get('/recorrido-virtual', [App\Http\Controllers\TuristaController::class, 'virtual'])->name('virtual');
Route::post('/encuesta', [App\Http\Controllers\TuristaController::class, 'encuesta'])->name('encuesta.store');

Route::get('/grafica-encuestas', [App\Http\Controllers\HomeController::class, 'grafica']);


// App::missing(function($exception)
// {
//     return Response::view('errors.missing', array(), 404);
// });

Route::group(['middleware' => 'auth'], function() {
    Route::get('/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
    Route::post('/users', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
    Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::get('/users/{user}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');
    Route::get('/users/{user}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::put('/users/{user}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
    Route::delete('/users/{user}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.delete');
    Route::resource('posts', App\Http\Controllers\PostController::class);
    Route::resource('permissions', App\Http\Controllers\PermissionController::class);
    Route::resource('roles', App\Http\Controllers\RoleController::class);

    Route::resource('comercios', App\Http\Controllers\ComercioController::class);
    Route::get('/comercio/{id}edit', [App\Http\Controllers\ComercioController::class, 'edit'])->name('comerciosedit');

    
    Route::get('/negocios', [App\Http\Controllers\NegocioController::class, 'index'])->name('negocios.index');
    Route::get('/negocios/{id}', [App\Http\Controllers\NegocioController::class, 'show'])->name('negocios.show');
    Route::get('/negocios/{id}/edit', [App\Http\Controllers\NegocioController::class, 'edit'])->name('negocios.edit');
    Route::put('/negocios/{id}', [App\Http\Controllers\NegocioController::class, 'update'])->name('negocios.update');
    Route::post('/negocios/file', [App\Http\Controllers\NegocioController::class, 'file'])->name('negocios.file');
   

});