var APP_DATA = {
  "scenes": [
    {
      "id": "0-dji_0636",
      "name": "DJI_0636",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -2.3960639621775925,
        "pitch": 1.5707963267948966,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -2.451313840891686,
          "pitch": 0.8060871670762513,
          "rotation": 9.42477796076938,
          "target": "1-dji_0609"
        },
        {
          "yaw": 1.3360170481862177,
          "pitch": 1.4949485951887471,
          "rotation": 3.141592653589793,
          "target": "11-dji_0626"
        },
        {
          "yaw": 2.262391033109279,
          "pitch": 0.6473606317642613,
          "rotation": 10.995574287564278,
          "target": "14-dji_0613"
        },
        {
          "yaw": -0.8204264533043215,
          "pitch": 0.6418515260332462,
          "rotation": 14.137166941154074,
          "target": "7-dji_0620"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.6269860774946796,
          "pitch": 0.38901789351862703,
          "title": "<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Puente Peatonal</font></font></font></font>",
          "text": "<div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Es un puente artesanal de madera, se</font></font></font></font></font></font></font></font></font></font></font></font></font></font><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">&nbsp;<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"> utiliza para la entrada y salida de personas locales y turistas.</font></font></font></font></font></font><br></font></font></font></font></font></font></font></font></font></font></font></font></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><br></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Este puente cuenta con 120 metros de largo y 2 metros de&nbsp;</font></font></font></font></font></font></font></font></font></font></font></font><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">ancho aproximadamente.&nbsp;</font></font></font></font></font></font></font></font></font></font></div><div><br></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Conecta el caserío Almendrales con la isla Tular.</font></font></font></font></font></font></font></font></font></font></font></font></div>"
        },
        {
          "yaw": 2.972517872817722,
          "pitch": 0.21551771973257416,
          "title": "<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Puente vehicular</font></font></font></font>",
          "text": "<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Obra de la municipalidad de La Blanca, San Marcos. </font><font style=\"vertical-align: inherit;\">Administración 2020-2024.</font></font></font></font><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><br></font></font></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Permite la entrada y salida de vehículos hacia la isla Tular, unicamente debera pagar ticket.</font></font></font></font></font></font></font></font></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><br></font></font></font></font></font></font></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><br></font></font></font></font></font></font></div>"
        }
      ]
    },
    {
      "id": "1-dji_0609",
      "name": "DJI_0609",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 1.1565211790622456,
        "pitch": -0.17612976775165912,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 1.12088709262831,
          "pitch": 0.06140666185932453,
          "rotation": 0,
          "target": "2-dji_0610"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-dji_0610",
      "name": "DJI_0610",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 1.473743243314102,
        "pitch": -0.053021518934045275,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 1.4246285326953778,
          "pitch": 0.04960304940373206,
          "rotation": 0,
          "target": "3-dji_0611"
        },
        {
          "yaw": -1.7165148227088416,
          "pitch": 0.056530036368359404,
          "rotation": 0,
          "target": "1-dji_0609"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-dji_0611",
      "name": "DJI_0611",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -0.013659098493871369,
        "pitch": 0.02885502722183375,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 3.1293821938849566,
          "pitch": 0.03001931459008489,
          "rotation": 0,
          "target": "2-dji_0610"
        },
        {
          "yaw": -0.01153039636790787,
          "pitch": 0.05279078730146303,
          "rotation": 0,
          "target": "4-dji_0612"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-dji_0612",
      "name": "DJI_0612",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 0.17105776429930586,
        "pitch": 0.030433350480727484,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -3.004033218537275,
          "pitch": 0.058100599834018496,
          "rotation": 0,
          "target": "3-dji_0611"
        },
        {
          "yaw": -1.2944765479485945,
          "pitch": 0.06245445130627836,
          "rotation": 0,
          "target": "5-dji_0618"
        },
        {
          "yaw": 1.6515712912487412,
          "pitch": 0.06045486576405068,
          "rotation": 0,
          "target": "12-dji_0616"
        },
        {
          "yaw": -0.2225810256078251,
          "pitch": -1.2438685848433586,
          "rotation": 0,
          "target": "0-dji_0636"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1846852628327511,
          "pitch": 0.0010089832895268103,
          "title": "<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">El Tular</font></font>",
          "text": "<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">La isla de Tular, también llamada playa de Tilapa,&nbsp;</font></font></font></font><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">se encuentra ubicado en el municipio de La Blanca, Departamento de San Marcos.</font></font><div><br></div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Tiene un aproximado de 1.8 KM de longitud y una superficie de 130,000 m² aproximadamente.</font></font></div><div><br></div><div><div><div><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Un lugar con vistas hacia el océano pacífico, con un ornato adecuado para el visitante, s</font></font></font><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">e puede dormir en apartamentos privados, comer en restaurantes.</font></font></font><br></div></div></div>"
        }
      ]
    },
    {
      "id": "5-dji_0618",
      "name": "DJI_0618",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 1.5118367960502912,
        "pitch": 0,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 0.09747200814113377,
          "pitch": 0.07440304279887222,
          "rotation": 0,
          "target": "4-dji_0612"
        },
        {
          "yaw": -2.955077102515544,
          "pitch": 0.007568620364473588,
          "rotation": 0,
          "target": "6-dji_0619"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-dji_0619",
      "name": "DJI_0619",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.5041179837977001,
        "pitch": 0.01127387275712799,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -2.975211813009775,
          "pitch": 0.03801146503301922,
          "rotation": 0,
          "target": "5-dji_0618"
        },
        {
          "yaw": 0.05860037658650796,
          "pitch": -0.0004415796149679352,
          "rotation": 0,
          "target": "7-dji_0620"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-dji_0620",
      "name": "DJI_0620",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.8443525586153022,
        "pitch": -0.2634357114881425,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -0.3832031750575222,
          "pitch": 0.085794468461458,
          "rotation": 0,
          "target": "8-dji_0623"
        },
        {
          "yaw": 1.1738345881363585,
          "pitch": 0.09375266734775067,
          "rotation": 0,
          "target": "6-dji_0619"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-dji_0623",
      "name": "DJI_0623",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.7625505664814902,
        "pitch": -0.13393780277678147,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 1.578143475816609,
          "pitch": 0.05305764879257602,
          "rotation": 0,
          "target": "7-dji_0620"
        },
        {
          "yaw": -0.1429215250988083,
          "pitch": 0.1454266590593427,
          "rotation": 0,
          "target": "9-dji_0624"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-dji_0624",
      "name": "DJI_0624",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.7395370516723894,
        "pitch": -0.19168904399101017,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 2.816514487285552,
          "pitch": -0.025403824049980983,
          "rotation": 0,
          "target": "8-dji_0623"
        },
        {
          "yaw": -0.21283934084766187,
          "pitch": 0.03995270039055221,
          "rotation": 0,
          "target": "10-dji_0625"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-dji_0625",
      "name": "DJI_0625",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 2.3794962252036633,
        "pitch": -0.1767792889187625,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -2.3963007648607118,
          "pitch": 0.044455775317420176,
          "rotation": 0,
          "target": "11-dji_0626"
        },
        {
          "yaw": 0.6537594573634635,
          "pitch": 0.06356984313342373,
          "rotation": 0,
          "target": "9-dji_0624"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-dji_0626",
      "name": "DJI_0626",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -0.04329147359237773,
        "pitch": -0.08845385045239595,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -1.9051498445713513,
          "pitch": -0.013417135191307494,
          "rotation": 0,
          "target": "16-dji_0627"
        },
        {
          "yaw": 1.5637663012150869,
          "pitch": 0.023286278048720632,
          "rotation": 0,
          "target": "10-dji_0625"
        },
        {
          "yaw": -0.02997464281200557,
          "pitch": 0.01963582645631945,
          "rotation": 0,
          "target": "0-dji_0636"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-dji_0616",
      "name": "DJI_0616",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.4192612439582746,
        "pitch": -0.06732839685090752,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -0.02158204734114655,
          "pitch": 0.045837905952749836,
          "rotation": 0,
          "target": "13-dji_0615"
        },
        {
          "yaw": 3.088473918439523,
          "pitch": 0.021764395581882923,
          "rotation": 0,
          "target": "4-dji_0612"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-dji_0615",
      "name": "DJI_0615",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -1.5916691741726297,
        "pitch": -0.11589535312177723,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 3.1057448180297778,
          "pitch": 0.04989129426264327,
          "rotation": 12.566370614359176,
          "target": "12-dji_0616"
        },
        {
          "yaw": -0.06516639242400757,
          "pitch": -0.0063743892603636,
          "rotation": 12.566370614359176,
          "target": "14-dji_0613"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "14-dji_0613",
      "name": "DJI_0613",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -0.02345852168964413,
        "pitch": -0.1457577401476513,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": 3.0935265327881716,
          "pitch": 0.07031903391987981,
          "rotation": 6.283185307179586,
          "target": "13-dji_0615"
        },
        {
          "yaw": -1.4997614712660674,
          "pitch": 0.023142578278198656,
          "rotation": 0,
          "target": "15-dji_0628"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "15-dji_0628",
      "name": "DJI_0628",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -3.076627101611921,
        "pitch": -0.13916802214794544,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -0.03331803555695778,
          "pitch": 0.011894430714164628,
          "rotation": 0,
          "target": "14-dji_0613"
        },
        {
          "yaw": 1.5072501466746289,
          "pitch": 0.09468891010955893,
          "rotation": 0,
          "target": "16-dji_0627"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "16-dji_0627",
      "name": "DJI_0627",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 0.9853858648997367,
        "pitch": -0.1301257265921265,
        "fov": 1.3123694188512842
      },
      "linkHotspots": [
        {
          "yaw": -0.5836739849618624,
          "pitch": 0.0404058192851231,
          "rotation": 0,
          "target": "15-dji_0628"
        },
        {
          "yaw": 2.3419044792481563,
          "pitch": 0.02524609177475412,
          "rotation": 0,
          "target": "11-dji_0626"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "tour-virual",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
