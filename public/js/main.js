function initMap(){
    const ubicacion = new Localizacion(()=>{
        const coords = {lat: 14.499706,lng: -92.185255 }
        var texto = '<h4>Lugar: Playa de Tilapa</h4>'+'<h6>Lugar Turistico ubicado en el Municipio de la Blanca, Departamento de San Marcos.</h6>';
        
        const options = {
            center:coords,
            zoom : 12,
           
        }

        var map = document.getElementById('map');

        const mapa = new google.maps.Map(map , options);

        const marcador =new google.maps.Marker({
            position:coords,
            map:mapa,
            title:'Playa de Tilapa'
        });

        var informacion = new google.maps.InfoWindow({
            content:texto
        });
        marcador.addListener('click',function(){
            informacion.open(mapa,marcador);
        })
    });
}