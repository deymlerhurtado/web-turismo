$(document).ready(function () {

    grafica();
});

function grafica(){
    let _url = '/grafica-encuestas';
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuesta = response;
            let total_=[0,0,0,0];
            let i=0;
            respuesta.forEach(respuestas => {
                total_[i]= respuestas.total;
                i++;
            });
            // console.log(total_);
            graph(total_);
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function graph(datos){

    var barCanvas = document.getElementById('barChart');
    var barChart = new Chart(barCanvas, {
        type: 'bar',
        data:{
            labels: ['Excelente','Bueno','Regular','Hay que mejorar'],
            datasets:[
                {
                    label:'Resultados',
                    data: datos,
                    backgroundColor:['green','blue','yellow','red']
                }
            ]  
        },
        options:{
            scales:{
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    })
}

    // $(function(){
    //     var datos = <?php echo json_encode($datas);?>;
    //     var barCanvas =  $("#barChart");
      
    //     var barChart = new Chart(barCanvas, {
    //         type: 'bar',
    //         data:{
    //             labels: ['Excelente','Bueno','Regular','Hay que mejorar'],
    //             datasets:[
    //                 {
    //                     label:'Incremento encuesta 2020',
    //                     data: datos,
    //                     backgroundColor:['green','blue','yellow','red']
    //                 }
    //             ]  
    //         },
    //         options:{
    //             scales:{
    //                 yAxes: [{
    //                     ticks: {
    //                         beginAtZero: true
    //                     }
    //                 }]
    //             }
    //         }
    //     })
    // });
