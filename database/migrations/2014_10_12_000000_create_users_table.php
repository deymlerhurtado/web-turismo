<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        
        Schema::create('estados', function (Blueprint $table) {
            $table->id();
            $table->string('estado');
        });    

        Schema::create('comercios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',50);
            $table->string('telefono',50);
            $table->unsignedBigInteger('id_estado')->default(1);//relacion estados
            $table->foreign('id_estado')->references('id')->on('estados');
            $table->unsignedBigInteger('id_user'); //relacion tipo de pago
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('comerciodetalles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_comercio'); //relacion tipo de pago
            $table->foreign('id_comercio')->references('id')->on('comercios');
            $table->string('servicio1',50)->nullable()->default('Restaurante Ejemplo');
            $table->string('servicio2',50)->nullable()->default('Picsina Ejemplo');
            $table->string('servicio3',50)->nullable()->default('Hotel Ejemplo');
            $table->string('descripcion',100)->nullable()->default('reseña de negocio');
            $table->string('contacto',50)->nullable()->default('502-5000-8080');
            $table->string('ruta')->nullable()->default('noimagen.png');
        });

        Schema::create('votos', function (Blueprint $table) {
            $table->id();
            $table->string('respuesta');
           
           
        });

        Schema::create('encuestas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_votos');//relacion estados
            $table->foreign('id_votos')->references('id')->on('votos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('estados');
        Schema::dropIfExists('comercios');
        Schema::dropIfExists('comerciodetalles');
    }
}
