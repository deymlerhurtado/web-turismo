<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Estado;
class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create([
            'id' => 1,
            'estado' => 'Activo'
        ]);

        Estado::create([
            'id' => 2,
            'estado' => 'Inactivo'
        ]);
    }
}
